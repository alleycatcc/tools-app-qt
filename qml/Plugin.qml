import QtQuick 2.10

Rectangle {
  function onKeyPressed (event, webview) {
  }
  function onBackPressed (event, webview) {
  }
  function onKeyReleased (event, webview) {
  }
  function onWebViewLoadStarted (loadRequest, webview) {
  }
  function onWebViewLoadCancelled (loadRequest, webview) {
  }
  function onWebViewLoadFailed (errorCode, errorString, loadRequest, webview) {
  }
  function onWebViewLoadSucceeded (loadRequest, webview) {
  }
  function onWebViewLoadUnknown (loadRequest, webview) {
  }
  function onWebViewComponentCompleted () {
  }
  function onWebViewMaximized () {
    console.log ('webview maximized')
  }

  Rectangle {
    id: pluginLightWatcher
    objectName: "pluginLightWatcher"

    property bool lightState: false

    // --- auto-generated listener based on name
    onLightStateChanged: {
      var state = lightState
      webview.runJavaScript (
        'window.hostChannelToJS.setLight(' + String (state) + ')',
        function () {}
      )
    }
  }
}
