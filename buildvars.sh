# --- provide this file to the build script with -c
# --- all keys must be present.

# --- iconLabel and webviewlocation must be overridden by the -variant
# scripts.
# --- all other values can be optionally overridden.

windowtitle='AlleyCat Tools'

# --- no hyphens: becomes the .so name too.
projectname=toolsapp

theme=cat-spinner
themever=1

qmltype=simple
qmlver=1

# ------ android manifest
_canonical_packagename=cc.alleycat.android.tools

# --- this one is for identifying unique apps on the device.

# it is also used for resolving `R`, so it seems it does have to match the
# actual package.
#
# there is probably a way to have both a test and a release variant on the
# device though.

manifestpackagename="$_canonical_packagename"
mainactivity_iconlabel=[VARIANT]
mainactivity_packagename="$_canonical_packagename".JNIActivity

# --- desktop static local
# webviewlocation=/webroot/index.html
# --- android static local
# webviewlocation=file:///android_asset/index.html
# --- remote http
# webviewlocation=http://67.209.116.246:3000
webviewlocation=[VARIANT]

qtrootdir=/maybe/qt
qtversion=5.10.1

projsrcdir="$projdir"/src
projqmldir="$projdir"/qml

qmakefileforeignpri="$projdir"/qmake/foreign.pri

srcfilesforeign=(
    "$projsrcdir"/catqt_foreign.h
    "$projsrcdir"/cat_tls_root.cpp
    "$projsrcdir"/cat_tls_root.h
)

# --- android.
buildvars-android () {
    srcforeignandroid="$projsrcdir"/android-sources

    android_home=/maybe/android/android-sdk
    android_ndk_root=/maybe/android/android-ndk/android-ndk-r10e
    androidapiversion=28
    android_ndk_host=linux-x86_64
    android_ndk_platform=android-16
    android_ndk_toolchain_prefix=arm-linux-androideabi
    android_ndk_toolchain_version=4.9
    android_ndk_tools_prefix=arm-linux-androideabi

    jdk=/usr/lib/jvm/java-8-openjdk-amd64

    keyurl=/home/fritz/private/release-key.jks
    keyalias=allen-alleycat
}
