#ifndef CATQT_FOREIGN_H
#define CATQT_FOREIGN_H

#define CATQT_FOREIGN_ROOT CatTlsRoot

#include "catqt_main.h"

#include "cat_tls_root.h"

#ifdef PLATFORM_ANDROID

// --- these are the 'reverse' bindings: make C++ functions available from
// Java.
//
// --- the other way around is simpler and doesn't require this sort of
// inversion of control.
//
// --- this is a static structure in a header file, because getting it
// through a method called on the 'foreign root' is tricky (it might not
// exist yet when `JNI_OnLoad` is called).

catqt_jniSpec catqt_foreign_jniSpec[] = {
    {
        strstr(JNI_CLASS),
        "jniTorchModeCallback",
        "(Z)V",
        reinterpret_cast<void *>(CATQT_FOREIGN_ROOT::torchModeCallbackJNI),
    },
};
#endif

#endif
