package cc.alleycat.android.tools;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import android.hardware.camera2.*;
import android.hardware.camera2.CameraManager.TorchCallback;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.R.id;
import android.os.Looper;
import android.os.Handler;

import org.qtproject.qt5.android.bindings.QtActivity;
import org.qtproject.qt5.android.QtLayout;
import org.qtproject.qt5.android.QtSurface;

import cc.alleycat.android.qtwebview.QtWebViewCommon;

public class JNIActivity extends QtActivity {
    private static final String TITLE = "ACatTools";
    private static final String TAG = "ACatTools.JNIActivity";

    private static final String HOST_CHANNEL_FROM_JS = "hostChannelFromJS";

    private static boolean initted = false;
    private static NotificationManager notificationManager;
    private static Notification.Builder builder;
    private static JNIActivity instance;

    private static CameraManager cameraManager;
    private static String cameraId;

    private static final int c_default_sound = android.app.Notification.DEFAULT_SOUND;
    private static final int c_default_vibrate = android.app.Notification.DEFAULT_VIBRATE;
    private static final int c_default_lights = android.app.Notification.DEFAULT_LIGHTS;
    private static final String c_category_alarm = android.app.Notification.CATEGORY_ALARM;
    private static final int c_priority_default = android.app.Notification.PRIORITY_DEFAULT;

    private static native void jniTorchModeCallback(boolean x);

    private static WebView webview;

    public JNIActivity() {
        instance = this;
    }

    public static boolean notify(String text, String tag, int id) {
        if (!initted) return errorf("notify: not initted");

        builder.setContentText(text);
        notificationManager.notify(tag, id, builder.build());
        return true;
    }

    public static boolean init() {
        Log.w(TAG, String.format("Beginning init"));

        webview = getWebView();
        if (webview == null) return errorf("init: can't get webview");

        initJSInterface(webview);

        notificationManager = (NotificationManager)instance.getSystemService(Context.NOTIFICATION_SERVICE);
        cameraManager = (CameraManager)instance.getSystemService(CameraManager.class);
        cameraId = getCameraId();

        setupTorchCallback(cameraManager);

        builder = new Notification.Builder(instance)
            .setContentTitle(TITLE)
            .setSmallIcon(R.drawable.icon)
            // xxx
            //.setLargeIcon(R.drawable.iconLarge)
            .setDefaults(c_default_sound | c_default_vibrate | c_default_lights)
            .setCategory(c_category_alarm)
            .setPriority(c_priority_default)
        ;
        Log.w(TAG, String.format("init successful"));
        initted = true;

        return true;
    }

    public static boolean pageLoaded() {
        if (!initted) return errorf("pageLoaded: not initted");
        QtWebViewCommon.pageLoaded(instance, webview);
        return true;
    }

    private static WebView getWebView() {
        View child;
        int childCount;
        View rootview = instance.findViewById(android.R.id.content);
        Log.w(TAG, String.format("root: %s", rootview.toString()));

        try {
            FrameLayout rootlayout = (FrameLayout)rootview;
            Log.w(TAG, String.format("root framelayout: %s", rootlayout.toString()));
            childCount = rootlayout.getChildCount();
            Log.w(TAG, String.format("num children of frameview: %d", childCount));
            child = rootlayout.getChildAt(0);
            Log.w(TAG, String.format("child frameview: %s", child.toString()));
            QtLayout qtlayout = (QtLayout)child;
            Log.w(TAG, String.format("qtlayout: %s", qtlayout.toString()));
            childCount = qtlayout.getChildCount();
            Log.w(TAG, String.format("num children of qtlayout: %d", childCount));

            child = qtlayout.getChildAt(0);
            Log.w(TAG, String.format("qtlayout child 0: %s", child.toString()));
            QtSurface qtsurface = (QtSurface)child;

            // --- note, don't try to get children of qtsurface: it's a view.
            Log.w(TAG, String.format("qtsurface: %s", qtsurface.toString()));

            child = qtlayout.getChildAt(1);
            Log.w(TAG, String.format("qtlayout child 1: %s", child.toString()));
        } catch (RuntimeException e) {
            Log.w(TAG, String.format("layout not as expected, can't find webview"));
            return null;
        }

        return (WebView)child;
    }

    private static void initJSInterface(final WebView webview) {
        Looper l = Looper.getMainLooper();
        Handler h = new Handler(l);
        h.post (new Runnable() {
            public void run() {
                webview.addJavascriptInterface(new JsInterface(), HOST_CHANNEL_FROM_JS);
            }
        });
    }

    private static void setupTorchCallback(CameraManager cameraManager) {
        TorchCallback tcb = new TorchCallback() {
            public void onTorchModeChanged(String cameraId, boolean enabled) {
                Log.w(TAG, String.format("torch callback: id %s was changed to %b", cameraId, enabled));
                jniTorchModeCallback(enabled);
            }
            public void onTorchModeUnavailable(String cameraId) {
                Log.w(TAG, String.format("torch callback: id %s has become unavailable", cameraId));
            }
        };

        Looper looper = Looper.getMainLooper();
        Handler handler = new Handler(looper);
        cameraManager.registerTorchCallback(tcb, handler);
    }

    private static String getCameraId() {
        String id = null;
        try {
            String[] cameraIds = cameraManager.getCameraIdList();
            if (cameraIds.length == 0) {
                Log.w(TAG, "No cameras found");
                return null;
            }
            // take the first .. ok? xxx
            id = cameraIds[0];
            CameraCharacteristics ch = cameraManager.getCameraCharacteristics(id);
            boolean hasFlash = ch.get(ch.FLASH_INFO_AVAILABLE);
            if (!hasFlash) {
                Log.d(TAG, "no flash unit");
                id = null;
            }
        } catch (CameraAccessException e) {
            Log.d(TAG, String.format("camera access exception: %s", e.toString()));
        }
        return id;
    }

    public static boolean setTorch(boolean state) {
        boolean ok = false;

        Log.w(TAG, "setTorch!");

        if (!initted) return errorf("setTorch: not initted");

        try {
            String id = cameraId;
            Log.d(TAG, String.format("id: %s", id));
            if (id == null) {
                Log.d(TAG, String.format("setTorch: no camera found, skipping"));
            } else {
                cameraManager.setTorchMode(id, state);
            }
            ok = true;
        } catch (CameraAccessException e) {
            Log.d(TAG, String.format("camera access exception: %s", e.toString()));
        } catch (IllegalArgumentException e) {
            Log.d(TAG, String.format("illegal argument exception: %s", e.toString()));
        }

        return ok;
    }

    private static boolean errorf(String err) {
        error(err);
        return false;
    }

    private static void error(String err) {
        Log.e(TAG, String.format("error: %s", err));
    }

    static class JsInterface {
//         private WebView view;
//         public JsInterface(WebView view) {
//             this.view = view;
//         }

        @JavascriptInterface
        public boolean setLight(boolean state) {
            return instance.setTorch(state);
        }
    }
}
