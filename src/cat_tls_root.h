#ifndef CAT_TOP_H
#define CAT_TOP_H

#include <vector>

#ifdef PLATFORM_ANDROID
# include <android/log.h>
# include <jni.h>
# include <QtAndroidExtras/QAndroidJniEnvironment>
# include <QtAndroidExtras/QAndroidJniObject>
#endif

#include <QQmlApplicationEngine>

#include "catqt_global.h"
#include "catqt_qml.h"

#define str(s) #s
#define strstr(s) str(s)

#define JNI_CLASS cc/alleycat/android/tools/JNIActivity

using std::vector;

class CatTlsRoot : public QObject
{
    Q_OBJECT

public:
    explicit CatTlsRoot(QObject *parent = nullptr);

    static CatTlsRoot *instance;
    void setQmlApplicationEngine(QQmlApplicationEngine *);
#ifdef PLATFORM_ANDROID
    static void torchModeCallbackJNI(JNIEnv *, jobject, jboolean);
#endif

// --- these should be slots in order to be called from qml.
public slots:
    bool notify(QString text, QString tag, int id);
    bool setLight(bool state);
    bool initActivity();
    bool pageLoaded();
#ifdef PLATFORM_ANDROID
    void torchModeCallback(bool b);
#endif

private:
    QQmlApplicationEngine *engine;
#ifdef PLATFORM_ANDROID
    bool notifyAndroid(QString text, QString tag, int id);
    bool setLightAndroid(bool state);
    bool initActivityAndroid();
    bool pageLoadedAndroid();
#endif

protected:
};

#endif
