#include <iostream>

#include "cat_tls_root.h"

CatTlsRoot *CatTlsRoot::instance = nullptr;

CatTlsRoot::CatTlsRoot(QObject *parent) : QObject(parent)
{
    instance = this;
}

void CatTlsRoot::setQmlApplicationEngine(QQmlApplicationEngine *engine)
{
    this->engine = engine;
}

bool CatTlsRoot::notify(QString text, QString tag, int id)
{
#ifdef PLATFORM_ANDROID
    return notifyAndroid(text, tag, id);
#else
    std::cout << "[mock] notify" << std::endl;
    return true;
#endif
}

bool CatTlsRoot::setLight(bool state)
{
#ifdef PLATFORM_ANDROID
    return setLightAndroid(state);
#else
    std::cout << "[mock] setLight" << std::endl;
    return true;
#endif
}

bool CatTlsRoot::initActivity()
{
#ifdef PLATFORM_ANDROID
    return initActivityAndroid();
#else
    std::cout << "[mock] initActivity" << std::endl;
    return true;
#endif
}

bool CatTlsRoot::pageLoaded()
{
#ifdef PLATFORM_ANDROID
    return pageLoadedAndroid();
#else
    std::cout << "[mock] pageLoaded" << std::endl;
    return true;
#endif
}

#ifdef PLATFORM_ANDROID
bool CatTlsRoot::notifyAndroid(QString text, QString tag, int id)
{
    jint jid = id;

    QAndroidJniObject a_text = QAndroidJniObject::fromString(text);
    QAndroidJniObject a_tag = QAndroidJniObject::fromString(tag);

    const char *signature = "(Ljava/lang/String;Ljava/lang/String;I)Z";

    jboolean ok = QAndroidJniObject::callStaticMethod<jboolean>(
        strstr(JNI_CLASS),
        "notify",
        signature,
        a_text.object<jstring>(),
        a_tag.object<jstring>(),
        jid
    );
    return (bool)ok;
}
#endif

#ifdef PLATFORM_ANDROID
bool CatTlsRoot::initActivityAndroid()
{
    const char *signature = "()Z";
    jboolean ok = QAndroidJniObject::callStaticMethod<jboolean>(
        strstr(JNI_CLASS),
        "init",
        signature
    );
    return (bool)ok;
}
#endif

#ifdef PLATFORM_ANDROID
bool CatTlsRoot::pageLoadedAndroid()
{
    const char *signature = "()Z";
    jboolean ok = QAndroidJniObject::callStaticMethod<jboolean>(
        strstr(JNI_CLASS),
        "pageLoaded",
        signature
    );
    return (bool)ok;
}
#endif

#ifdef PLATFORM_ANDROID
bool CatTlsRoot::setLightAndroid(bool state)
{
    const char *signature = "(Z)Z";

    jboolean ok = QAndroidJniObject::callStaticMethod<jboolean>(
        strstr(JNI_CLASS),
        "setTorch",
        signature,
        state
    );
    return (bool)ok;
}
#endif

// --- note: this will probably result in at least one 'no root window'
// warning, depending on when the callback is fired (the timing is trick_y).

#ifdef PLATFORM_ANDROID
void CatTlsRoot::torchModeCallback(bool b)
{
    catqt_debug(QString ("CatTlsRoot: torch is %1").arg(b));
    catqt_debug(QString ("CatTlsRoot: setting object property pluginLightWatcher to %1").arg(b));
    catqt_qml_setQMLPropertyOnChildOfRootWindow("pluginLightWatcher", "lightState", b);
}
#endif

#ifdef PLATFORM_ANDROID
void CatTlsRoot::torchModeCallbackJNI(JNIEnv *env, jobject thiz, jboolean b)
{
    Q_UNUSED(env)
    Q_UNUSED(thiz)
    catqt_debug(QString("callback from java: torch state is %1").arg(b));

    if (!CatTlsRoot::instance) {
        catqt_debug(QString("torchModeCallback(): instance is null"));
        return;
    }

    CatTlsRoot::instance->torchModeCallback((bool)b);
}
#endif
